class CoursesController < ApplicationController
  before_action :set_course, only: [:edit, :update, :destroy]

  def index
    @course = Course.all
  end

  def new
    @course = Course.new
  end

  def create
    @course = Course.new(course_params)
    if @course.save
      redirect_to root_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @course.update(course_params)
      redirect_to root_path
    else
      redirect_to edit_course_path(@course)
    end
  end

  def destroy
    @course = Course.find(params[:id])
    if @course.destroy
      redirect_to root_path
    end
  end

  def show
  end

  private

  def set_course
    @course = Course.find(params[:id])
  end

  def course_params
    params.require(:course).permit(:courseID, :course_name, :group_id)
  end
end
