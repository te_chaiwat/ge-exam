class CreateCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :courses do |t|
      t.string :courseID
      t.string :course_name
      t.integer :group_id

      t.timestamps
    end
  end
end
