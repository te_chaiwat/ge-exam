# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Group.create(id: 1, group_name: 'กลุ่มวิชาภาษาศาสตร์')
Group.create(id: 2, group_name: 'กลุ่มวิชาสังคมศาสตร์')
Group.create(id: 3, group_name: 'กลุ่มวิชามนุษย์ศาสตร์')
Group.create(id: 4, group_name: 'กลุ่มวิชาวิทยาศาสตร์และเทคโนโลยี')

